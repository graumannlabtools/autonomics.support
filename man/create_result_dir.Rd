% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/create_result_dir.R
\name{create_result_dir}
\alias{create_result_dir}
\title{Create result dir}
\usage{
create_result_dir(projectdir = getwd(), subdir = "", showWarnings = TRUE)
}
\arguments{
\item{projectdir}{project directory}

\item{subdir}{results subdir}

\item{showWarnings}{passed to dir.create (logical)}
}
\value{
path to result dir
}
\description{
Given projectdir 'root/analysis/collaborator/project', 
create resultdir 'root/results/collaborator/project/subdir', 
and return that path.
}
