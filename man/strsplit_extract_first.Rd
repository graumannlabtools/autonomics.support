% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/strsplit_extract_first.R
\name{strsplit_extract_first}
\alias{strsplit_extract_first}
\title{Strsplit and extract first}
\usage{
strsplit_extract_first(x, sep)
}
\arguments{
\item{x}{collapsed character vector}

\item{sep}{separator}
}
\value{
uncollapsed character vector
}
\description{
Strsplit and extract first
}
